package com.fedcorp;

import com.fedcorp.SAX.ParserSAX;
import com.fedcorp.planesLibrary.Plane;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File file = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\xml\\planes.xml");

//        ParserDOM p = new ParserDOM();
        ParserSAX p = new ParserSAX();

        List<Plane> list = p.parseFromXML(file);

        for (Plane plane : list){
            System.out.println(plane);
        }
    }
}
