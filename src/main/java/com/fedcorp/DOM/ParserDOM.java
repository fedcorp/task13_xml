package com.fedcorp.DOM;

import com.fedcorp.planesLibrary.Chars;
import com.fedcorp.planesLibrary.Plane;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParserDOM {

    public List<Plane> parseFromXML(File file) {
        List<Plane> list = new ArrayList<>();
        Document document = null;
        DocumentBuilder builder;
        Plane plane;

        if (file != null) {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                builder = factory.newDocumentBuilder();
                document = builder.parse(file);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("'file' variable does not exist");
            return null;
        }

        Element root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            plane = new Plane();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                plane.setModel(element.getElementsByTagName("model").item(0).getTextContent());
                plane.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                plane.setParams(splitParameters(element.getElementsByTagName("parameters").item(0)));
                plane.setChars(splitChars(element.getElementsByTagName("chars").item(0)));
                plane.setPrice(Double.valueOf(element.getElementsByTagName("price").item(0).getTextContent()));

                list.add(plane);
            }
        }
        return list;
    }

    private HashMap<String, Double> splitParameters(Node n) {
        HashMap<String, Double> map = new HashMap();
        NodeList nodeList;

        if (n.hasChildNodes()) {
            nodeList = n.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    if (element.getTagName().equals("length")) {
                        map.put("length", Double.valueOf(element.getTextContent()));
                    }
                    if (element.getTagName().equals("width")) {
                        map.put("width", Double.valueOf(element.getTextContent()));
                    }
                    if (element.getTagName().equals("height")) {
                        map.put("height", Double.valueOf(element.getTextContent()));
                    }
                }
            }
        }
        return map;
    }

    private Chars splitChars(Node n) {
        Chars chars = new Chars();
        NodeList nodeList;
        if (n.hasChildNodes()) {
            nodeList = n.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) node;
                    if (element.getTagName().equals("ammo")) {
                        chars.setAmmo(Integer.valueOf(element.getTextContent()));
                    }
                    if (element.getTagName().equals("type")) {
                        chars.setPlaneType(element.getTextContent());
                    }
                    if (element.getTagName().equals("radar")) {
                        chars.setRadar(element.getTextContent());
                    }
                    if (element.getTagName().equals("seating")) {
                        chars.setSitsNum(Integer.valueOf(element.getTextContent()));
                    }
                }
            }
        }
        return chars;
    }

}
