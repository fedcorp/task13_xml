package com.fedcorp.planesLibrary;

import java.util.HashMap;

public class Plane {
    private String model;
    private String origin;
    private Chars chars;
    private HashMap<String, Double> params;
    private double price;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    public HashMap<String, Double> getParams() {
        return params;
    }

    public void setParams(HashMap<String, Double> params) {
        this.params = params;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Plane" + '\n' +
                "   model = " + model + '\n' +
                "   origin = " + origin + '\n' +
                "   chars = " + chars + '\n' +
                "   parameters = " +
                "length - " + params.get("length") +
                ", width - " + params.get("width") +
                ", height - " + params.get("height") + '\n' +
                "   price = " + price + " millions" + '\n'
                ;
    }
}
