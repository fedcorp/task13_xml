package com.fedcorp.SAX;

import com.fedcorp.planesLibrary.Chars;
import com.fedcorp.planesLibrary.Plane;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SAXHandler extends DefaultHandler {

    private Plane plane;
    private Chars chars;
    private HashMap<String, Double> map;
    private List<Plane> planeList = new ArrayList<>();

    private boolean isModel = false;
    private boolean isOrigin = false;
    private boolean isChars = false;
    private boolean isParams = false;
    private boolean isType = false;
    private boolean isSits = false;
    private boolean isAmmo = false;
    private boolean isRadar = false;
    private boolean isLength = false;
    private boolean isWidth = false;
    private boolean isHeight = false;
    private boolean isPrice = false;

    @Override
    public void startElement (String uri, String localName,
                              String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("plane")){
            plane = new Plane();

        }

        else if (qName.equalsIgnoreCase("model")){isModel = true;}
        else if (qName.equalsIgnoreCase("origin")){isOrigin = true;}
        else if (qName.equalsIgnoreCase("chars")){isChars = true;}
        else if (qName.equalsIgnoreCase("type")){isType = true;}
        else if (qName.equalsIgnoreCase("seating")){isSits = true;}
        else if (qName.equalsIgnoreCase("ammo")){isAmmo = true;}
        else if (qName.equalsIgnoreCase("radar")){isRadar = true;}
        else if (qName.equalsIgnoreCase("parameters")){isParams = true;}
        else if (qName.equalsIgnoreCase("length")){isLength = true;}
        else if (qName.equalsIgnoreCase("width")){isWidth = true;}
        else if (qName.equalsIgnoreCase("height")){isHeight = true;}
        else if (qName.equalsIgnoreCase("price")){isPrice = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("chars")){
            plane.setChars(chars);
        }
        if (qName.equalsIgnoreCase("parameters")){
            plane.setParams(map);
        }
        if (qName.equalsIgnoreCase("plane")){
            planeList.add(plane);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (isModel){
            plane.setModel(new String(ch, start, length));
            isModel = false;
        }
        else if (isOrigin){
            plane.setOrigin(new String(ch, start, length));
            isOrigin = false;
        }
        else if (isChars){
            chars = new Chars();
            isChars = false;
        }
        else if (isParams){
            map = new HashMap<>();
            isParams = false;
        }
        else if(isType){
            chars.setPlaneType(new String(ch, start, length));
            isType = false;
        }
        else if (isSits){
            chars.setSitsNum(Integer.valueOf(new String(ch, start, length)));
            isSits = false;
        }
        else if (isAmmo){
            chars.setAmmo(Integer.valueOf(new String(ch, start, length)));
            isAmmo = false;
        }
        else if (isRadar){
            chars.setRadar(new String(ch, start, length));
            isRadar = false;
        }
        else if (isLength){
            map.put("length",Double.valueOf(new String(ch, start, length)));
            isLength = false;
        }
        else if (isWidth){
            map.put("width",Double.valueOf(new String(ch, start, length)));
            isWidth = false;
        }
        else if(isHeight){
            map.put("height",Double.valueOf(new String(ch, start, length)));
            isHeight = false;
        }
        else if (isPrice){
            plane.setPrice(Double.valueOf(new String(ch, start, length)));
            isPrice = false;
        }
    }

    public List<Plane> getPlaneList() {
        return planeList;
    }
}
